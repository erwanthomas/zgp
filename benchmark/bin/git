#!/usr/bin/env sh

set -o errexit
set -o nounset

CMD="$*"
WORKDIR="$(cd "$(dirname "$0")" && pwd -P)"

if [ -z "${FAKE_DELAY:-}" ]
then
  FAKE_DELAY=0.5
fi

warn () {
  echo "$@" >&2
}

abort () {
  warn "$@"
  exit 1
}

fake_delay () {
  if [ "$FAKE_DELAY" -gt 0 ]
  then
    sleep "$FAKE_DELAY"
  fi
}

git_status_v1 () {
  fake_delay
  cat "$WORKDIR/large_git_status_v1.txt"
}

git_status_v2 () {
  fake_delay
  cat "$WORKDIR/large_git_status_v2.txt"
}

git_revparse () {
  fake_delay
}

case "$CMD" in
  "rev-parse --git-dir")
    ;;

  "status --branch --porcelain=v1")
    git_status_v1
    ;;

  "status --branch --porcelain=v2")
    git_status_v2
    ;;

  "rev-parse --short HEAD")
    git_revparse
    ;;

  *)
    abort "Unknown command: $CMD"
    ;;
esac
