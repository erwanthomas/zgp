# frozen_string_literal: true

require "benchmark"
require "fileutils"
require "json"
require "shellwords"
require "time"

module BM
  class Measure
    def self.from_json(json)
      data = JSON.parse(json, symbolize_names: true)
      data[:timestamp] = Time.iso8601(data[:timestamp]).utc
      new(**data)
    end

    def initialize( # rubocop:disable Metrics/ParameterLists
      utime:,
      stime:,
      cutime:,
      cstime:,
      real:,
      label:,
      timestamp:
    )
      @tms = Benchmark::Tms.new(utime, stime, cutime, cstime, real)
      @label = label
      @timestamp = timestamp.utc
      freeze
    end

    %i[utime stime cutime cstime real total].each do |timing|
      define_method(timing) { @tms.public_send(timing) }
    end

    attr_reader :label, :timestamp

    def to_json(*)
      JSON.pretty_generate(
        utime:     utime,
        stime:     stime,
        cutime:    cutime,
        cstime:    cstime,
        real:      real,
        label:     label,
        timestamp: timestamp.iso8601
      )
    end
  end

  class MeasureStore
    PATH = File.expand_path("../measures", __dir__).freeze
    MAX_RECORDS = 10

    def self.setup(name)
      path = File.join(PATH, name)
      FileUtils.mkdir_p(path)
      new(path)
    end

    def self.setup!(name)
      path = File.join(PATH, name)
      raise "Invalid store: #{name}" unless Dir.exist?(path)

      new(path)
    end

    def initialize(path = PATH)
      raise ArgumentError, "Not a directory: #{path}" unless Dir.exist?(path)

      @path = path
    end

    def slice(*args)
      paths = Dir.glob(File.join(@path, "*.json"))
      paths.sort! { |a, b| b <=> a }

      paths = paths.slice(*args)

      case paths
      when Array
        paths.map { |path| Measure.from_json(File.read(path)) }
      when String
        Measure.from_json(File.read(paths))
      end
    end

    def save(measure)
      record_path = File.join(@path, "#{measure.timestamp.strftime("%Y%m%d%H%M%S")}.json")
      record = measure.to_json

      File.write(record_path, record)
    end

    def clean(all: false)
      ko = Dir.glob(File.join(@path, "*.json"))
      return ko if ko.empty?

      unless all
        ko.sort! { |a, b| b <=> a }
        ko.slice!(0, MAX_RECORDS)
      end

      File.unlink(*ko) unless ko.empty?

      ko
    end
  end

  class MeasureTable
    class Column
      def self.[](header)
        case header
        when :index
          IndexColumn.new(header)
        when :utime, :stime, :cutime, :cstime, :real, :total
          MsColumn.new(header)
        when :label
          LimitedLengthColumn.new(header, maxlen: 70, etc: "...")
        else
          new(header)
        end
      end

      def initialize(header)
        @header = header
        @len = header.length
      end

      def header_fmt
        "%-#{@len}s"
      end

      def cell_fmt
        "%-#{@len}s"
      end

      def sep_fmt
        "-" * @len
      end

      def build_val(measure, _x, _y)
        measure.public_send(@header).to_s
      end

      def cell(measure, x, y)
        build_val(measure, x, y).tap do |val|
          len = val.to_s.length
          @len = len if len > @len
        end
      end
    end

    class IndexColumn < Column
      def cell_fmt
        "%0#{@len}d"
      end

      def build_val(_measure, _x, y)
        y
      end
    end

    class LimitedLengthColumn < Column
      CONTINUE = "..."

      def initialize(header, maxlen:, etc: "")
        super(header)
        @maxlen = maxlen
        @etc = etc
      end

      def build_val(*)
        str = super

        if str.length > @maxlen
          str.slice(0, @maxlen - @etc.length) + @etc
        else
          str
        end
      end
    end

    class MsColumn < Column
      PRECISION = 7

      def header_fmt
        "%-#{@len}s"
      end

      def cell_fmt
        "%-#{@len}.#{PRECISION}f"
      end

      def build_val(measure, _x, _y)
        measure.public_send(@header) * 1000
      end

      def cell(measure, x, y)
        build_val(measure, x, y).tap do |val|
          len = val.to_i.to_s.length + 1 + PRECISION
          @len = len if len > @len
        end
      end
    end

    HEADERS = %i[index label timestamp utime stime cutime cstime real total].freeze

    def initialize(*measures)
      @measures = measures
    end

    def to_s # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity
      cols = HEADERS.map { |h| Column[h] }
      rows =
        @measures.map.with_index(1) do |measure, y|
          cols.map.with_index(0) do |col, x|
            col.cell(measure, x, y)
          end
        end

      hed_fmt = format(" %s \n", cols.map(&:header_fmt).join(" | "))
      row_fmt = format(" %s \n", cols.map(&:cell_fmt).join(" | "))
      row_sep = format("-%s-\n", cols.map(&:sep_fmt).join("-+-"))

      buf = +""
      buf << format(hed_fmt, *HEADERS)
      buf << row_sep
      rows.each { |row| buf << format(row_fmt, *row) }

      buf.freeze
    end
  end

  def self.call(task, iterations) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    tms = Benchmark.measure do
      iterations.times { task.run }
    end

    tms /= iterations

    measure = Measure.new(
      utime:     tms.utime,
      stime:     tms.stime,
      cutime:    tms.cutime,
      cstime:    tms.cstime,
      real:      tms.real,
      label:     task.label,
      timestamp: Time.now
    )

    store = MeasureStore.setup(task.store)
    store.save(measure)
    store.clean

    previous = store.slice(1..-1)

    table = MeasureTable.new(measure, *previous)

    puts table
  end
end
