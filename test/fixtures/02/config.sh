function setup {
  mkdir local remote

  pushd remote
    git init --bare
  popd

  pushd local
    git init
    git remote add origin ../remote
    git commit --allow-empty --message commit_foo
    git push --quiet --set-upstream origin master

    for i in $(seq 1 3)
    do
      echo $i > file0
      git add file0
      git commit --message commit_foo
    done

    echo 4 > file0
  popd
}

function teardown {
  rm -rf local remote
}
