function setup {
  mkdir local remote

  pushd remote
    git init --bare
  popd

  pushd local
    git init
    git remote add origin ../remote
    git commit --allow-empty --message commit_foo
    git push --quiet --set-upstream origin master
    touch file0 file1 file2
    git add file0
    git commit --message commit_foo
    echo foo > file0
  popd
}

function teardown {
  rm -rf local remote
}

