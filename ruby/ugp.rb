#!/usr/bin/env -S ruby --disable-all
# frozen_string_literal: true

module Ugp
  class Status
    def initialize
      @oid       = nil
      @head      = nil
      @branch    = nil
      @ahead     = 0
      @behind    = 0
      @index     = 0
      @unmerged  = 0
      @worktree  = 0
      @untracked = 0
    end

    attr_accessor :oid, :head

    attr_reader(
      :ahead,
      :behind,
      :index,
      :unmerged,
      :worktree,
      :untracked
    )

    def branch
      @branch || @head || @oid.slice(0, 7)
    end

    attr_writer :branch

    def inc_ahead(n)
      @ahead += n
    end

    def inc_behind(n)
      @behind += n
    end

    def inc_index(n)
      @index += n
    end

    def inc_unmerged(n)
      @unmerged += n
    end

    def inc_worktree(n)
      @worktree += n
    end

    def inc_untracked(n)
      @untracked += n
    end

    def clean?
      @index.zero? && @unmerged.zero? && @worktree.zero? && @untracked.zero?
    end
  end

  class StringStatusLineParser
    UPSTREAM = /\+(\d+) -(\d+)/

    def initialize(status)
      @status = status
    end

    def call(line) # rubocop:disable Metrics/PerceivedComplexity, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/AbcSize
      if line.start_with?("1") || line.start_with?("2")
        xy = line.slice(2, 2)
        @status.inc_index(1) unless xy.start_with?(".")
        @status.inc_worktree(1) unless xy.end_with?(".")

      elsif line.start_with?("?")
        @status.inc_untracked(1)

      elsif line.start_with?("u")
        @status.inc_unmerged(1)

      elsif line.start_with?("# branch.oid")
        oid = line.slice(13..-2)
        @status.oid = oid if oid != "(initial)"

      elsif line.start_with?("# branch.head")
        head = line.slice(14..-2)
        @status.head = head if head != "(detached)"

      elsif line.start_with?("# branch.ab")
        UPSTREAM.match(line) do |m|
          @status.inc_ahead(Integer(m[1]))
          @status.inc_behind(Integer(m[2]))
        end
      end
    end
  end

  class StringStatusParser
    def call(lines)
      status = Status.new
      parser = StringStatusLineParser.new(status)

      lines.each do |line|
        parser.call(line)
      end

      status
    end
  end

  class Options
    def self.parse(argv) # rubocop:disable Metrics/PerceivedComplexity, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/AbcSize
      opts = new
      return opts if argv.empty?

      i = 0
      max = argv.size

      while i < max
        key = argv[i]&.start_with?("--") ? argv[i] : nil
        break unless key

        j = i + 1

        if argv[j].nil? || argv[j].start_with?("--")
          val = ""
          i = j
        else
          val = argv[j]
          i = j + 1
        end

        case key
        when "--format"    then opts.format    = val
        when "--reset"     then opts.reset     = val
        when "--prefix"    then opts.prefix    = val
        when "--suffix"    then opts.suffix    = val
        when "--branch"    then opts.branch    = val
        when "--ahead"     then opts.ahead     = val
        when "--behind"    then opts.behind    = val
        when "--separator" then opts.separator = val
        when "--index"     then opts.index     = val
        when "--unmerged"  then opts.unmerged  = val
        when "--worktree"  then opts.worktree  = val
        when "--untracked" then opts.untracked = val
        when "--clean"     then opts.clean     = val
        end
      end

      opts
    end

    def initialize
      @format    = ""
      @reset     = ""
      @prefix    = ""
      @suffix    = ""
      @branch    = ""
      @ahead     = ""
      @behind    = ""
      @separator = ""
      @index     = ""
      @unmerged  = ""
      @worktree  = ""
      @untracked = ""
      @clean     = ""
    end

    attr_accessor(
      :format,
      :reset,
      :prefix,
      :suffix,
      :branch,
      :ahead,
      :behind,
      :separator,
      :index,
      :unmerged,
      :worktree,
      :untracked,
      :clean
    )
  end

  module Format
    COMPONENT = /<[abcdefghijklmnopqrstuvwxyz]+>/

    def self.call(fmt_str, values, buffer = +"") # rubocop:disable Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/AbcSize
      until fmt_str.empty?
        head, match, tail = fmt_str.partition(COMPONENT)

        buffer << head unless head.empty?

        break if match.empty?

        buffer <<
          case match
          when "<prefix>"     then values.prefix
          when "<suffix>"     then values.suffix
          when "<branch>"     then values.branch
          when "<ahead>"      then values.ahead
          when "<behind>"     then values.behind
          when "<separator>"  then values.separator
          when "<index>"      then values.index
          when "<unmerged>"   then values.unmerged
          when "<worktree>"   then values.worktree
          when "<untracked>"  then values.untracked
          when "<clean>"      then values.clean
          else
            match
          end

        fmt_str = tail
      end

      buffer
    end
  end

  class Components
    def initialize(status, opts)
      @status = status
      @opts = opts
    end

    def prefix
      "#{@opts.prefix}#{@opts.reset}"
    end

    def suffix
      "#{@opts.suffix}#{@opts.reset}"
    end

    def separator
      "#{@opts.separator}#{@opts.reset}"
    end

    def clean
      @status.clean? ? "#{@opts.clean}#{@opts.reset}" : ""
    end

    def branch
      "#{@opts.branch}#{@status.branch}#{@opts.reset}"
    end

    def ahead
      count(@opts.ahead, @status.ahead, @opts.reset)
    end

    def behind
      count(@opts.behind, @status.behind, @opts.reset)
    end

    def index
      count(@opts.index, @status.index, @opts.reset)
    end

    def unmerged
      count(@opts.unmerged, @status.unmerged, @opts.reset)
    end

    def worktree
      count(@opts.worktree, @status.worktree, @opts.reset)
    end

    def untracked
      count(@opts.untracked, @status.untracked, @opts.reset)
    end

    private

    def count(prefix, value, suffix)
      value.zero? ? "" : "#{prefix}#{value}#{suffix}"
    end
  end

  def self.git_available?
    system("git", "rev-parse", "--git-dir", %i[out err] => :close) == true
  end

  def self.git_status(&block)
    value = IO.popen(["git", "status", "--branch", "--porcelain=v2"], err: :close, &block)

    raise "Git status command failed" unless $?.success?

    value
  end

  def self.call(argv = [])
    return puts unless git_available?

    opts = Options.parse(argv)
    parser = StringStatusParser.new
    status = git_status { |io| parser.call(io) }
    components = Components.new(status, opts)

    puts Format.call(opts.format, components)
  end

  def self.main(argv)
    call(argv)
  rescue StandardError => e
    warn("#{e} [#{e.backtrace.first}]")
    exit 1
  else
    exit 0
  end
end

Ugp.main(ARGV) if $0 == __FILE__
